
//3.

db.hotelDB.insertOne( {
    name: "single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities.",
    roomsAvailable: 10,
    isAvailable: false
}
)


//4.
db.hotelDB.insertMany([
{
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation.",
    rooms_available: 5,
    isAvailable: false
},
{
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway.",
    rooms_available: 15,
    isAvailable: false
}
])


//5.

db.hotelDB.find(
{ name: "double" }
)

//6.

db.hotelDB.updateOne(
 { name: "queen"},
 {
     $set :  { rooms_available: 0}
 }
 )
         

//7.

db.hotelDB.deleteMany({
    rooms_available:0}
    )